﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Framework
{
    public class CollisionDetection
    {
        static CollisionDetection objs;
        private CollisionDetection(){}
        
        public static CollisionDetection getInstance()
        {
            if (objs == null)
            {
                objs = new CollisionDetection();
            }
            return objs;
        }

        public void enemyCollision(List<PictureBox> enemy, Reaction.effect react)     
        {
            for (int x = 0; x < enemy.Count; x++)
            {
                for (int y = 0; y < enemy.Count; y++)
                {
                    if (y != x)
                    {
                        if (enemy[x].Bounds.IntersectsWith(enemy[y].Bounds))
                        {
                            Reaction r;
                            r = Reaction.getInstance();
                            r.collisonReaction(Reaction.effect.directionChange,enemy[x],enemy[y]);
                        }
                    }
                }
            }
        }

        public void enemyCollision(PictureBox enemy1 , PictureBox enemy2, Reaction.effect react)
        {
            if (enemy1.Bounds.IntersectsWith(enemy2.Bounds))
            {
                Reaction r;
                r = Reaction.getInstance();
                r.collisonReaction(Reaction.effect.directionChange,enemy1,enemy2);
            }
        }


        public void playerCollision(List<PictureBox> enemy,PictureBox player,List<PictureBox> hurdles)  //1
        {
            foreach (var item in enemy)
            {
                if ((player.Bounds.IntersectsWith(item.Bounds)))
                {
                    Reaction r;
                    r = Reaction.getInstance();
                    r.collisonReaction(Reaction.effect.directionChange,player,item);
                }
            }
            foreach (var item in hurdles)
            {
                if (player.Bounds.IntersectsWith(item.Bounds))
                {
                    Reaction r;
                    r = Reaction.getInstance();
                    r.collisonReaction(Reaction.effect.directionChange,player,item);
                }
            }
        }


        public bool playerCollision(List<PictureBox> enemy, PictureBox player, List<PictureBox> hurdles, Reaction.effect react)  //-1
        {
            foreach (var item in enemy)
            {
                if ((player.Bounds.IntersectsWith(item.Bounds)))
                {
                    return true;
                }
            }
            foreach (var item in hurdles)
            {
                if (player.Bounds.IntersectsWith(item.Bounds))
                {
                    return true;
                }
            }
            return false;
        }

        public bool playerCollision(PictureBox player, PictureBox enemy, PictureBox hurdle)   //2
        {
            if (player.Bounds.IntersectsWith(enemy.Bounds) || (player.Bounds.IntersectsWith(hurdle.Bounds)))
            {
                return true;
            }
            return false;
        }

        public void playerCollision(PictureBox player, PictureBox enemy, PictureBox hurdle, Reaction.effect react)  //-2
        {
            if (player.Bounds.IntersectsWith(enemy.Bounds) || (player.Bounds.IntersectsWith(hurdle.Bounds)))
            {
                Reaction r;
                r = Reaction.getInstance();
                r.collisonReaction(Reaction.effect.directionChange, player, enemy);
            }
        }

        public bool playerCollision(PictureBox player , PictureBox objects)   //3
        {
            if (player.Bounds.IntersectsWith(objects.Bounds))
            {
                return true;
            }
            return false;
        }

        public bool playerCollision(PictureBox player, PictureBox objects, Reaction.effect react)   //-3
        {
            bool isAlive = true;
            if (player.Bounds.IntersectsWith(objects.Bounds))
            {
                Reaction r;
                r = Reaction.getInstance();
                isAlive = r.collisonReaction(react, player,objects);
            }
            return isAlive;
        }
        
        public void outOfsite(PictureBox objs)
        {
            int y = objs.Top;
            int x =  objs.Left;
            if (objs.Left > 1166)
            {
                objs.Location = new Point(1, y);
            }
            else if (objs.Left < 10)
            {
                objs.Location = new Point(1166, y);
            }
            else if (objs.Top < 1)
            {
                objs.Location = new Point(x, 400);
            }
            else if (objs.Top > 400)
            {
                objs.Location = new Point(x, 1);
            }
        }
        
        public void playerTravel(int playerY,PictureBox objs)
        {
            int y = playerY;
            objs.Location = new Point(1166, y);
        }

        public void outOfsite(int leftLimit , int rightLimit , int topLimit , int bottamLimit , PictureBox objs)  //for user define boundries
        {
            int x = objs.Left;
            int y = objs.Top;
            if (objs.Left > leftLimit)
            {
                objs.Location = new Point(rightLimit , y);
            }
            else if (objs.Left < rightLimit)
            {
                objs.Location = new Point(leftLimit , y);
            }
            else if (objs.Top < topLimit)
            {
                objs.Location = new Point( x , bottamLimit);
            }
            else if (objs.Top > bottamLimit)
            {
                objs.Location = new Point( x ,topLimit);
            }
        }

        public void outOfsite(PictureBox objs, int newX ,int newY)           //for track of the player
        {
            int y = objs.Top;
            int x = objs.Left;
            if (objs.Left > 1166)
            {
                objs.Location = new Point(10, newY);
            }
            else if (objs.Left < 10)
            {
                objs.Location = new Point(1166, newY);
            }
            else if (objs.Top < 1)
            {
                objs.Location = new Point(newX, 400);
            }
            else if (objs.Top > 400)
            {
                objs.Location = new Point(newX, 1);
            }
        }
    }
}
