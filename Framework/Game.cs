﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framework
{
    public class Games
    {
        private static Games objects;
        private static Form game;
        public static List<GameObjects> pictureBox = new List<GameObjects>();

        private Games(string path /*, Form g*/)
        {
            //game = g;
            Tile t;
            t = Tile.getInstance(path);
        }

        private Games(/*, Form g*/)
        {
            //game = g;
            Tile t;
            t = Tile.getInstance();
        }

        //private Games()
        //{
        //    //game = g;
        //    Tile t;
        //    t = Tile.getInstance();
        //}

        public void addRecord(GameObjects obj)
        {
            pictureBox.Add(obj);
            //game.Controls.Add(obj.getPictureBox());
        }

        public Form createMap(Form game , string path)
        {
            Tile t = Tile.getInstance(path);
            game =  t.createMap(game);
            return game;
        }

        public static  Games etinstance(string path /*, Form g*/)
        {
            if (objects == null)
            {
                objects = new Games(path/*,g*/);
            }
            return objects;
        }

        public static Games etinstance(/*, Form g*/)
        {
            if (objects == null)
            {
                objects = new Games(/*g*/);
            }
            return objects;
        }

        //public static Games etinstance()
        //{
        //    if (objects == null)
        //    {
        //        objects = new Games();
        //    }
        //    return objects;
        //}

        public void updateEnemy()
        {
            int playerY = 200;
            for (int i = 0; i < pictureBox.Count; i++)
            {
                GameObjects s = pictureBox[i];
                CollisionDetection c;
                PictureBox p = s.getPictureBox();
                int speed = s.getspeed();
                if (s.GetTypeofObj() == ObjectType.objectType.player)
                {
                    playerY = p.Top;
                }
                else if(s.GetTypeofObj() == ObjectType.objectType.enemy)
                {
                    Movement state = s.getmove();
                    FactoryMovement.move S = s.getstate();
                    s.updateMovement(p, speed, state);
                    c = CollisionDetection.getInstance();
                    if (p.Name == "pictureBox4" && p.Left < 10)
                    {
                        c.playerTravel(playerY, p);
                    }
                    else
                    {
                        c.outOfsite(p);
                    }
                }
            }
        }

        public void updatePlayer(KeyEventArgs e, int speed ,PictureBox picture)
        {
            KeyBoardControle.move state;
            state = check_state(e);
            KeyBoardControle k = new KeyBoardControle(); 
            k.get_state(speed,picture,state);
            CollisionDetection c = CollisionDetection.getInstance();
            c.outOfsite(picture);
        }

        public KeyBoardControle.move check_state(KeyEventArgs e)
        {
            KeyBoardControle.move m = KeyBoardControle.move.up;
            if (e.KeyCode == Keys.Up)
            {
                m = KeyBoardControle.move.up;
            }
            if (e.KeyCode == Keys.Down)
            {
                m = KeyBoardControle.move.down;
            }
            if (e.KeyCode == Keys.Left)
            {
                m = KeyBoardControle.move.left;
            }
            if (e.KeyCode == Keys.Right)
            {
                m = KeyBoardControle.move.right;
            }
            return m;
        }
    }
}
