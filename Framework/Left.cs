﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framework
{
    public class Left : Movement
    {
        static Left l;
        static int s;
        public static int value = 4;


        public int getvalue()
        {
            return value;
        }

        private Left()
        {
            s += 1;
        }

        
        public static Left getinstance()
        {
            if (l == null )
            {
                l = new Left();
            }
            return l;
        }
       

        public void movement(int speed, PictureBox picture)
        {
            picture.Left += speed;
        }
        //public Movement newState()
        //{
        //    //return new Right();
        //}

        public int getindex()
        {
            return s;
        }
    }
}