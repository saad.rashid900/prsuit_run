﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Framework
{
    public class Tile
    {
        static int enemyNO = 1;
        static string path;
        static int idx = 1;
        string[,] map = new string[10, 13]; 
        private static Tile t;
        internal Tile(string adress)
        {
            if(idx == 1)
            {
                //createArray(adress);
                path = adress;
            }
        }

        public static Tile getInstance()
        {
            if (t == null)
            {
                t = new Tile(path);
            }
            return t;
        }

        public static Tile getInstance(string path)
        {
            if(t == null)
            {
                t = new Tile(path);
            }
            return t;
        }
        private void createArray(string path)
        {
            idx = idx + 1;
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            string str = "";
            str = sr.ReadLine();
            int y = 0;
            int x = 0;
            string array;
            while (x < 10)
            {
                array = "";
                if (str != null)
                {
                    for (int idx = 0; (idx < str.Length) ; idx++)
                    {
                        if (str[idx] == ',')
                        {
                            map[x, y] = array;
                            y = y + 1;
                            array = "";
                        }
                        else
                        {
                            array = array + str[idx];
                        }
                    }
                }
                x = x + 1;
                y = 0;
                str = sr.ReadLine();
            }
        }
        public Form createMap(Form Game)
        {
            string m;
            Game.BackgroundImage = new Bitmap($"F:/testing/Game/Game/Resources/x.jpg");
            Game.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Tile;
            Game.MaximumSize = new System.Drawing.Size(1300, 500);
            Game.MinimumSize = new System.Drawing.Size(1300, 500);

            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 13; y++)
                {
                    m = map[x,y];
                    PictureBox p = new PictureBox();
                    p.Location = new Point(x*100,y*50);
                    p.Size = new System.Drawing.Size(100,50);
                    p.BackgroundImage = new Bitmap($"F:/testing/Game/Game/Resources/{m}.jpg");
                    p.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
                    Game.Controls.Add(p);
                }
            }
            return Game;
        }

        public PictureBox createPictureBox(int tileX,int tileY,string path, ObjectType.objectType objtype) 
        {
            PictureBox pic = new PictureBox();
            pic.BackgroundImage = new Bitmap($"F:/testing/Game/Game/Resources/{path}.jpg");
            pic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            pic.Size = new Size(100,50);
            pic.Location = new Point(tileX*100,tileY*50);
            if (objtype == ObjectType.objectType.enemy)
            {
                pic.Tag = "Enemy";
                pic.Name = $"Enemy{enemyNO.ToString()}";
                enemyNO += 1;
            }
            else if (objtype == ObjectType.objectType.player)
            {
                pic.Tag = "Player";
                pic.Name = "Player";
            }
            return pic;
        }
    }
}
