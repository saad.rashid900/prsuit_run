﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;

namespace Framework
{
    public class Factory
    {
        static Factory objects;
        static int[] object_types = new int[2];
        private Factory(){}
        
        public static Factory createInstance()
        {
            if (objects == null)
            {
                objects = new Factory();
            }
            return objects;
        }

        //public GameObjects load(int tileX,int tileY,string path, int speed, FactoryMovement.move state, ObjectType.objectType objtype)
        //{
        //    int s = (int)objtype;
        //    object_types[s] += 1;
        //    GameObjects obj = new GameObjects(int tileX, int tileY, string path, speed, state,objtype);
        //    return obj;
        //}


        //public GameObjects load(int tileX, int tileY, string path, int speed, ObjectType.objectType objtype)
        //{
        //    int s = (int)objtype;
        //    object_types[s] += 1;
        //    GameObjects obj = new GameObjects(int tileX, int tileY, string path, speed,objtype);
        //    return obj;
        //}

        public GameObjects load(PictureBox picture, int speed,FactoryMovement.move state,ObjectType.objectType objtype)
        {
            int s = (int)objtype;
            object_types[s] += 1;  
            GameObjects obj = new GameObjects(picture,speed, state, objtype);
            return obj;
        }

        
        public GameObjects load(PictureBox picture, int speed, ObjectType.objectType objtype)
        {
            int s = (int)objtype;
            object_types[s] += 1;
            GameObjects obj = new GameObjects(picture, speed, objtype);
            return obj;
        }
    }
}