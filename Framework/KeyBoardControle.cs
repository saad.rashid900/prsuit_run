﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framework
{
    public class KeyBoardControle : Movement 
    {
        public static int value = 5;
        public enum move
        {
            up = 1,
            down = 2,
            left = 3,
            right = 4
        }

        public void movement(int speed, PictureBox picture)
        {
        }
        
        public int getvalue()
        {
            return value;
        }

        public void get_state(int speed, PictureBox picture,move m)
        {
            int s = (int)m;
            if ( s == 1)
            {
                picture.Top -= speed;
            }
            if (s == 2)
            {
                picture.Top += speed;
            }
            if (s == 3)
            {
                picture.Left -= speed;
            }
            if (s == 4)
            {
                picture.Left += speed;
            }
        }
    }
}   