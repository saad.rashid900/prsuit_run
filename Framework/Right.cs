﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framework
{
    public class Right : Movement
    {
        public static int value = 3;
        static Right r;

        private Right(){}

        public void movement(int speed , PictureBox picture)
        {
            picture.Left -= speed;
        }

        public int getvalue()
        {
            return value;
        }

        public static Right getinstance()
        {
            if (r == null)
            {
                r = new Right();
            }
            return r;
        }

        //public Movement newState()
        //{
        //    //return new Left();
        //}
    }
}
