﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    public class FactoryMovement
    {
        static FactoryMovement f;
        static List<Movement> availableMovements = new List<Movement>();
        static List<Movement> occupiedMovements = new List<Movement>();
        private FactoryMovement()
        {
        }

        public static FactoryMovement getInstance()
        {
            if( f == null)
            {
                f = new FactoryMovement();
            }
            return f;
        }
        
        public enum move
        {
            upward = 1,
            backward = 2,
            right = 3,
            left = 4,
            keyBoardMovement =5
        }

        //public Movement getsource(move movement)
        //{
        //    Movement state = checkAvailable(movement);
        //    if (state == null)
        //    {
        //        state = newMovement(movement);
        //    }
        //    occupiedMovements.Add(state);
        //    return state;
        //}

        //public int release(Movement state)
        //{
        //    int idx = 0;
        //    for (int i = 0; i < occupiedMovements.Count; i++)
        //    {
        //        if (state == occupiedMovements[i])
        //        {
        //            availableMovements.Add(occupiedMovements[i]);
        //            occupiedMovements[i] = null;
        //        }
        //    }
        //    for (int i = 0; i < availableMovements.Count; i++)
        //    {
        //        idx = i;
        //    }
        //    return idx;
        //}

        //public Movement checkAvailable(move movement)
        //{
        //    Movement state;
        //    int value = (int)movement;
        //    for (int i = 0; i < availableMovements.Count; i++)
        //    {
        //        if (availableMovements[value] == null)
        //        {
        //            state = newMovement(movement);
        //        }
        //        else if (value == availableMovements[i].getvalue())
        //        {
        //            return availableMovements[i];
        //        }
        //    }
        //    return null;
        //}

        public Movement newMovement(move movement)
        {
            int value = (int)movement;
            if (value == Upward.value)
            {
                return Upward.getinstance();
            }

            if (value == Backward.value)
            {

                return Backward.getinstance();
               
            }

            if (value == Right.value)
            {
                return Right.getinstance();
            }

            if (value == Left.value)
            {
                return Left.getinstance();
            }
            if (value == KeyBoardControle.value)
            {
                return new KeyBoardControle();
            }
            return null;
        }

        public Movement getSource(move movement)
        {
            Movement state = check(movement);
            if (state == null)
            {
                state = newMovement(movement);
            }
            return state;
        }

        public Movement check(move movement)
        {
            int value = (int)movement;
            for (int i = 0; i < availableMovements.Count; i++)
            {
                if (availableMovements[i].getvalue() == value)
                {
                    Movement m = availableMovements[i];
                    availableMovements.RemoveAt(i);
                    occupiedMovements.Add(m);
                    return m;
                }
            }
            return null;
        }

        public int release(Movement state)
        {
            int idx = 0;
            for (int i = 0; i < occupiedMovements.Count; i++)
            {
                if (state.getvalue() == occupiedMovements[i].getvalue())
                {
                    availableMovements.Add(occupiedMovements[i]);
                    occupiedMovements.RemoveAt(i);
                }
            }
            for (int i = 0; i < availableMovements.Count; i++)
            {
                idx = i;
            }
            return idx;
        }
    }
}

