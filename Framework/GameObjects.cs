﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Framework
{
    public class GameObjects
    {
        ObjectType.objectType type;
        PictureBox Pic;
        int Speed;
        FactoryMovement.move state;
        Movement move;

        //internal GameObjects(int tileX,int tileY,string path, int speed, FactoryMovement.move state, ObjectType.objectType objtype)
        //{

        //    this.Pic = getPic(tileX,tileY,path);
        //    this.Speed = speed;
        //    this.state = state;
        //    this.type = objtype;
        //    this.move = get_move(state);
        //}

        //private PictureBox getPic(int tileX,int tileY,string path,ObjectType.objectType objtype)
        //{
        //    PictureBox pic = new PictureBox();
        //    Tile t = Tile.getInstance();
        //    pic =  t.createPictureBox(tileX,tileY,path,objtype);
        //    return Pic;
        //}

        //internal GameObjects(int tileX, int tileY, string path,PictureBox pic, int speed, ObjectType.objectType objtype)
        //{
        //    this.Pic = getPic(tileX, tileY, path);
        //    this.Speed = speed;
        //    this.state = FactoryMovement.move.left;
        //    this.move = get_move(FactoryMovement.move.left);
        //    this.type = objtype;
        //}

        internal GameObjects(PictureBox pic, int speed, FactoryMovement.move state, ObjectType.objectType objtype)
        {
            this.Pic = pic;
            this.Speed = speed;
            this.state = state;
            this.move = get_move(state);
            this.type = objtype;
        }

        public ObjectType.objectType GetTypeofObj()
        {
            return this.type;
        }

        internal GameObjects(PictureBox pic, int speed, ObjectType.objectType objtype)
        {
            this.Pic = pic;
            this.Speed = speed;
            this.state = FactoryMovement.move.left;
            this.move = get_move(FactoryMovement.move.left);
        }

        private Movement get_move(FactoryMovement.move state)
        {
            FactoryMovement obj;
            obj = FactoryMovement.getInstance();
            Movement mave = obj.getSource(state);
            return mave;
        }


        public PictureBox getPictureBox()
        {
            return this.Pic;
        }


        public int getspeed()
        {
            return this.Speed;
        }


        public Movement getmove()
        {
            return this.move;
        }


        public FactoryMovement.move getstate()
        {
            return this.state;
        }

        public void updateMovement(PictureBox picture, int left,Movement move)
        {
            move.movement(left, picture);   
        }

        ~GameObjects()
        {
            FactoryMovement f;
            f = FactoryMovement.getInstance();
            int m =f.release(this.move);
            MessageBox.Show("destructor invoked" + m.ToString());
        }
    }
}