﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framework
{
    public class Backward : Movement
    {
        static public int value = 2;
        static Backward b;
        private Backward(){}
        public static Backward getinstance()
        {

            if (b == null)
            {
                b = new Backward();
            }
            return b;
        }
        public void movement(int speed, PictureBox picture)
        {
            picture.Top -= speed;
        }

        public int getvalue()
        {
            return value;
        }

        //public Movement newState()
        //{
        //    return new Upward();
        //}
    }
}
