﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;

namespace Framework
{
    public class Reaction
    {
        private Reaction() { }
        static Reaction objs;

        public enum effect 
        {
            health = 1,
            gameOver = 2,
            directionChange = 3,
            destroy = 4
        }

        public static Reaction getInstance()
        {
            if(objs == null)
            {
                objs = new Reaction();
            }
            return objs;
        }
        public bool collisonReaction(effect react , PictureBox pic1 , PictureBox pic2)
        {
            if (react == effect.destroy)
            {
                pic1.Dispose();
                if ((pic1.Tag.ToString()) == "Player")
                {
                    react = effect.gameOver;
                }
            }
            else if(react == effect.gameOver)
            {
                pic1.Dispose();
                return false;
            }
            else if(react == effect.directionChange)
            {

            }
            else if (react == effect.health)
            {

            }
            return true;
        }
    }
}
