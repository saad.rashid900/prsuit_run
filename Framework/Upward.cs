﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Framework
{
    public class Upward : Movement
    {
        public static int value = 1;
        static Upward u;
        private Upward() { }

        public static Upward getinstance()
        {

            if (u == null)
            {
                u = new Upward();
            }
            return u;
        }

        public int getvalue()
        {
            return value;
        } 

        public void movement(int speed , PictureBox picture)
        {
            picture.Top += speed;
        }

        //public Movement newState()
        //{
        //    return new Backward();
        //}   
    }
}
