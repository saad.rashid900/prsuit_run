﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    class Enemy
    {
        private static Enemy objects;
        public static ArrayList pictureBox = new ArrayList();
        private Enemy()
        {
        }
        
        public  void addRecord(EnemyObject enemy)
        {
            pictureBox.Add(enemy);
        }
        
        public static Enemy getinstance()
        {
            if(objects == null)
            {
                objects = new Enemy();
            }
            return objects;
        }
        public  void updateEnemy()
        {
            for (int i = 0; i < pictureBox.Count; i++)
            {
                EnemyObject s = (EnemyObject) pictureBox[i];
                PictureBox p = s.getPictureBox();
                int speed = s.getspeed();
                Movement state = s.getstate();
                s.updateMovement(p,speed,state);               
                
            }
        }
    }
}

//if (idx == 0)
//{
//    pictureBox[0] = enemy;
//}
//if (idx == 1)
//{
//    pictureBox[1] = enemy;
//}
//if (idx == 2)
//{
//    pictureBox[2] = enemy;
//}
//idx = idx + 1;
