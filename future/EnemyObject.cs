﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Game
{
    class EnemyObject
    {
        PictureBox Pic;
        int Speed;
        static int objectCounter = 0;
        Movement state;
        public enum movements
        {
            forwad,
            backward,
            left,
            right
        }
       
       public EnemyObject(PictureBox pic , int speed ,Movement state)
       {
            objectCounter += 1;
            this.Pic = pic;
            this.Speed = speed;
            this.state = state;
       }
       
        
        public EnemyObject(PictureBox pic , int speed)
        {
            this.Pic = pic;
            this.Speed = speed;
            this.state = new Left();
        }
        public PictureBox getPictureBox()
        {
            return this.Pic;
        }

        public int getspeed()
        {
            return this.Speed;
        }

        public Movement getstate()
        {
            return this.state;
        }

        public int counter_object()
        {
            return objectCounter;
        }
        
        public void updateMovement(PictureBox picture ,int left,Movement state)
        {
            state.movement(left,picture);
        }
    }
}